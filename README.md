# Spring State Machine

## Que es una maquina de estados

Una máquina de estados es un programa que nos sirve para determinar el comportamiento de algo en base al estado en el que se encuentre. Pueden ser finitas (si los estados lo son) o infinitas (eran teóricas, hasta la aparición de las maquinas cuanticas)

## Estados
Un estado representa el comportamiento de un elemento que hace algo durante cierto tiempo.

## Transición
Una transición es el paso de un estado de origen a otro de destino. Tiene siempre asociada una condición y puede requerir la ejecución de alguna acción.

## Evento
Es la acción que hace que la maquina, por medio de una transición, cambie de estado

## Ejercicio
Repliquemos este flujo!
![ejercicio.png](https://bytebucket.org/somospnt/1403-spring-state-machine/raw/6b481662b6972f64a85ac043519295bc8d17bc58/ejercicio.png)