package com.ssm.demo;

import com.ssm.demo.enums.Estado;
import com.ssm.demo.enums.Evento;
import static org.assertj.core.api.Java6Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.test.StateMachineTestPlan;
import org.springframework.statemachine.test.StateMachineTestPlanBuilder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SsmApplicationTests {

    @Autowired
    private StateMachine<Estado, Evento> stateMachine;

    @Before
    public void setup() {
        stateMachine.stop();
    }
    
    @Test
    public void maquinaDeEstados_empiezaEnOrdenadoConPlanDeDosPasosCorrectos_quedaEnEntregado() throws Exception {
        StateMachineTestPlan<Estado, Evento> plan = StateMachineTestPlanBuilder.<Estado, Evento>builder()
                .stateMachine(stateMachine)
                .defaultAwaitTime(0)
                .step()
                .expectState(Estado.ORDENADO)
                .and()
                .step()
                .sendEvent(Evento.ARMAR)
                .expectStateChanged(1)
                .expectState(Estado.ARMADO)
                .and()
                .step()
                .sendEvent(Evento.ENTREGAR)
                .expectStateChanged(1)
                .expectState(Estado.ENTREGADO)
                .and()
                .build();
        plan.test();
        assertThat(stateMachine.isComplete()).isFalse();
    }

    @Test
    public void maquinaDeEstados_empiezaEnOrdenadoConPlanCompleto_terminaEnPagado() throws Exception {
        StateMachineTestPlan<Estado, Evento> plan = StateMachineTestPlanBuilder.<Estado, Evento>builder()
                .stateMachine(stateMachine)
                .defaultAwaitTime(0)
                .step()
                .expectState(Estado.ORDENADO)
                .and()
                .step()
                .sendEvent(Evento.ARMAR)
                .expectStateChanged(1)
                .expectState(Estado.ARMADO)
                .and()
                .step()
                .sendEvent(Evento.ENTREGAR)
                .expectStateChanged(1)
                .expectState(Estado.ENTREGADO)
                .and()
                .step()
                .sendEvent(Evento.HACER_FACTURA)
                .expectStateChanged(1)
                .expectState(Estado.FACTURADO)
                .and()
                .step()
                .sendEvent(Evento.PAGO_RECIBIDO)
                .expectStateChanged(1)
                .expectState(Estado.PAGADO)
                .and()
                .build();
        plan.test();
        assertThat(stateMachine.isComplete()).isTrue();
    }

    @Test
    public void maquinaDeEstados_empiezaEnOrdenadoConPlanConCancelacion_terminaEnCancelado() throws Exception {
        StateMachineTestPlan<Estado, Evento> plan = StateMachineTestPlanBuilder.<Estado, Evento>builder()
                .stateMachine(stateMachine)
                .defaultAwaitTime(1)
                .step()
                .expectState(Estado.ORDENADO)
                .and()
                .step()
                .sendEvent(Evento.CANCELAR)
                .expectStateChanged(1)
                .expectState(Estado.CANCELADO)
                .and()
                .build();
        plan.test();
        assertThat(stateMachine.isComplete()).isFalse();
    }

}
