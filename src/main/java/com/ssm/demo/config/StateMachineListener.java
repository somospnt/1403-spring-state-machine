package com.ssm.demo.config;

import com.ssm.demo.enums.Estado;
import com.ssm.demo.enums.Evento;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;

public class StateMachineListener extends StateMachineListenerAdapter<Estado, Evento> {

    @Override
    public void stateChanged(State<Estado, Evento> from, State<Estado, Evento> to) {
        System.out.println("El estado de la orden cambio a " + to.getId());
    }

}
