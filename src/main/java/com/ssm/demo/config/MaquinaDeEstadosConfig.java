package com.ssm.demo.config;

import com.ssm.demo.enums.Estado;
import com.ssm.demo.enums.Evento;
import java.util.EnumSet;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

@Configuration
@EnableStateMachine
public class MaquinaDeEstadosConfig extends EnumStateMachineConfigurerAdapter<Estado, Evento> {

    @Override
    public void configure(StateMachineStateConfigurer<Estado, Evento> states) throws Exception {
        states
                .withStates()
                .states(EnumSet.allOf(Estado.class));
    }

    @Override
    public void configure(StateMachineConfigurationConfigurer<Estado, Evento> config) throws Exception {
        config
                .withConfiguration();
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<Estado, Evento> transitions) throws Exception {
    }
}
