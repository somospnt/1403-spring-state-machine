package com.ssm.demo.enums;

public enum Estado {
  ORDENADO, ARMADO, ENTREGADO, FACTURADO, PAGADO, CANCELADO, DEVUELTO
}
