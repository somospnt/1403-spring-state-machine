package com.ssm.demo.enums;

public enum Evento {
    ORDENAR, ARMAR, ENTREGAR, HACER_FACTURA, PAGO_RECIBIDO, CANCELAR, RECLAMAR, REARMAR
}
