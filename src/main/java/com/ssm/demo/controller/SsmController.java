package com.ssm.demo.controller;

import com.ssm.demo.enums.Estado;
import com.ssm.demo.enums.Evento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ssm")
public class SsmController {

    @Autowired
    private StateMachine<Estado, Evento> stateMachine;

    @GetMapping("/reset")
    public void reset() {
        stateMachine.stop();
        stateMachine.start();
    }

    @GetMapping("/{evento}")
    public boolean avanzar(@PathVariable String evento) throws Exception {
        return stateMachine.sendEvent(Evento.valueOf(evento.toUpperCase()));
    }
    
    @GetMapping("/estado")
    public Estado estado() throws Exception {
        return stateMachine.getState().getId();
    }

}
